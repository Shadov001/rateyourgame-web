import React from "react";
import app from "../base";

const Home = () => {
    return (
        <div className="main_content">
            <h1>Welcome {app.auth().currentUser.email}!</h1>
            <button onClick={() => app.auth().signOut()}>Sign out</button>
        </div>
    );
};

export default Home;