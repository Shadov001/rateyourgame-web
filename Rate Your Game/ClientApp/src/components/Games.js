import React, { Component } from 'react';
import Game from './Game/Game';

export class Games extends Component {

    constructor(props) {
        super(props);
        this.state = {
            allUsers: [],
            filteredGames: [],
            loading: true,
        };

        fetch('api/Game/GetGames')
            .then(response => response.json())
            .then(data => {
                this.setState({ allUsers: data, loading: false });
            });

        this.filterGames = this.filterGames.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    static insertGame(game) {
        fetch('api/Game/PostGame', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(game)
        });
    }

     static renderGamesTable(filteredGames) {
        return (
            filteredGames.map(game =>
                <div className="column" key={game.id}>
                    <Game
                        id={game.id}
                        description={game.description}
                        title={game.title}
                        views={game.views}
                        studioName={game.studioName}
                        releaseDate={game.releaseDate}
                        genre={game.genre} />
                </div>
            )
        );
    }

    filterGames(text) {
        this.setState({
            filteredGames: this.state.allUsers.filter(game => game.title.toLowerCase().includes(text.toLowerCase()))
        }
        );
    }

    handleChange(event) {
        this.filterGames(event.target.value);
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading all games...</em></p>
            : (this.state.filteredGames.length > 0  ? Games.renderGamesTable(this.state.filteredGames) : Games.renderGamesTable(this.state.allUsers));

        return (
            <div className="gamesContainer">
                <input onChange={this.handleChange} />
                <div className="row">
                    {contents}
                </div>
            </div>
        );
    }
}
