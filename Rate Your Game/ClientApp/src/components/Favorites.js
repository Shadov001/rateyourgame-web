﻿import React from "react";
import app from "../base";

const Favorites = () => {
    return (
        <div className="main_content">
            This is list of your favorite games, {app.auth().currentUser.email}!
        </div>
    );
};

export default Favorites;