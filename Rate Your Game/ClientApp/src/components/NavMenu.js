import React, { Component } from 'react';
import { NavLink } from 'reactstrap';
import './styles.css';
import { Link } from "react-router-dom";
import app from "../base";

export class NavMenu extends Component {
    static displayName = NavMenu.name;

    constructor(props) {
        super(props);

        this.toggleNavbar = this.toggleNavbar.bind(this);
        this.state = {
            collapsed: true
        };
    }

    toggleNavbar() {
        this.setState({
            collapsed: !this.state.collapsed
        });
    }

    render() {
        return (    
            <div className="wrapper">
                <div className="sidebar">
                    <h2>Rate Your Game</h2>
                    <ul>
                        <li><NavLink tag={Link}  to="/home">Home</NavLink></li>
                        <li><NavLink tag={Link}  to="/games">Games</NavLink></li>
                        <li><NavLink tag={Link} to="/rankings">Rankings</NavLink></li>
                        {app.auth().currentUser ? (<li><NavLink tag={Link} to="/favorites">Your favorites</NavLink></li>) : null }
                        <li><NavLink tag={Link}  to="/about">About</NavLink></li>
                    </ul>

                    <div class="foot">
                        <a> <NavLink tag={Link} to="/login">{app.auth().currentUser ? app.auth().currentUser.email.substring(0, app.auth().currentUser.email.lastIndexOf("@")) : "Login or register"}</NavLink> </a>
                    </div>
                </div>
            </div>
        );
    }
}   
