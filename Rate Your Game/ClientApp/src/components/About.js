﻿import React, { Component } from 'react';

export class About extends Component {

    constructor(props) {
        super(props);
        this.state = {};
        // pamiętaj żeby bindować metody zmieniające stan (this.xyz = this.xyz.bind(this))
    }

    render() {
        return (
            <div class="main_content">
                <div class="about">
                    <h1>About Rate Your Game</h1>
                    <h3>The Rate Your Game application is intended to help users (fans of video games) in choosing the best titles,
                        offering a rich library of games developed by the community and will allow to share their impressions after
                        the game. The application allows for issuing opinions that all users will have an insight into, as well as for
                        grading the game, which will have a later impact on its position in the generally available ranking, for which
                        an extensive algorithm has been designed. This algorithm is influenced by various factors such as user activity
                        on the site and the number of occurrences of games on the "Favorites" lists of all users.
                    </h3>
                    <ul class="social-icons">
                        <li title="Facebook"><a class="facebook" href="https://www.facebook.com"><i class="fa fa-facebook"></i></a></li>
                        <li title="Instagram"><a class="instagram" href="https://www.instagram.com/"><i class="fa fa-instagram"></i></a></li>
                        <li title="Snapchat"><a class="snapchat" href="https://www.snapchat.com/"><i class="fa fa-snapchat"></i></a></li>
                        <li title="Twitter"><a class="twitter" href="https://twitter.com/"><i class="fa fa-twitter"></i></a></li>
                        <li title="Youtube"><a class="youtube" href="https://youtube.com/"><i class="fa fa-youtube"></i></a></li>
                    </ul>
                </div>
            </div>
        );
    }
}