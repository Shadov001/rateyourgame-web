﻿import React from 'react';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

export default class Game extends React.Component {

    formatTime(time) {
        return new Date(time).toLocaleDateString();
    }

    render() {
        return (
            <Card className="gameRoot">
                <CardActionArea>
                    <CardMedia
                        component="img"
                        alt="Game image"
                        height="200"
                        witdh="250px"
                        image="2.jpg"
                    />
                    <CardContent style={{ backgroundColor: "#d9d9d9" }}>
                        <Typography gutterBottom variant="h5" component="h2">
                            {this.props.title}
                        </Typography>
                        <Typography color="textSecondary" component="p">
                            <strong>{this.props.genre}</strong>
                        </Typography>
                        <Typography color="textSecondary" component="p">
                            {this.props.studioName}
                        </Typography>
                        <Typography color="textSecondary" component="p">
                            <strong>Views:</strong> {this.props.views} 
                        </Typography>
                        <Typography color="textSecondary" component="p">
                            <strong>Released</strong> {this.formatTime(this.props.releaseDate)}
                        </Typography>
                    </CardContent>
                </CardActionArea>
                <CardActions style={{ backgroundColor: "#d9d9d9" }}>
                    <Button size="small" color="textSecondary">
                        Read more
                    </Button>
                    <Button size="small" color="textSecondary">
                        Add to favourites
                    </Button>
                </CardActions>
            </Card>
        );
    }
}
