import React, { useCallback, useContext } from "react";
import { withRouter, Redirect } from "react-router";
import app from "../base.js";
import { AuthContext } from "../Auth.js";
import {
    MDBContainer,
    MDBRow,
    MDBCol,
    MDBCard,
    MDBCardBody,
    MDBModalFooter,
    MDBIcon,
    MDBCardHeader,
    MDBBtn,
    MDBInput
} from "mdbreact";


const Login = ({ history }) => {
    const handleLogin = useCallback(
        async event => {
            event.preventDefault();
            const { email, password } = event.target.elements;
            try {
                await app
                    .auth()
                    .signInWithEmailAndPassword(email.value, password.value);
                history.push("/home");
            } catch (error) {
                alert(error);
            }
        },
        [history]
    );

    const { currentUser } = useContext(AuthContext);

    if (currentUser) {
        return <Redirect to="/home" />;
    }

    return (
        <div className = "signInForm">
            <MDBContainer>
                <MDBRow>
                    <MDBCol md="6">
                        <MDBCard>
                            <MDBCardBody>
                                <MDBCardHeader className="form-header deep-blue-gradient rounded">
                                    <h3 className="my-3">
                                        <MDBIcon icon="lock" /> Login:
                                    </h3>
                                </MDBCardHeader>
                                <form onSubmit={handleLogin}>
                                    <div className="grey-text">
                                        <MDBInput label="Type your email"  group type="email" name="email" validate error="wrong" success="right" />
                                        <MDBInput label="Type your password"  group type="password" name="password" validate />
                                    </div>

                                    <div className="text-center mt-4" >
                                        <MDBBtn color="light-blue" className="mb-3" type="submit" >
                                            Login
                                        </MDBBtn>
                                    </div>
                                </form>
                                <MDBModalFooter>
                                    <div className="font-weight-light">
                                        <p> <MDBBtn onClick={() => history.push("/signup")}>Not a member? Sign up</MDBBtn></p>
                                        <p> <MDBBtn>Forgot password?</MDBBtn> </p>
                                    </div>
                                </MDBModalFooter>
                            </MDBCardBody>
                        </MDBCard>
                    </MDBCol>
                </MDBRow>
            </MDBContainer>
        </div>
    );
};

export default withRouter(Login);