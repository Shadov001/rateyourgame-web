﻿import React, { useCallback } from "react";
import { withRouter } from "react-router";
import app from "../base";
import {
    MDBContainer,
    MDBRow,
    MDBCol,
    MDBCard,
    MDBCardBody,
    MDBModalFooter,
    MDBIcon,
    MDBCardHeader,
    MDBBtn,
    MDBInput
} from "mdbreact";

const SignUp = ({ history }) => {
    const handleSignUp = useCallback(async event => {
        event.preventDefault();
        const { email, password } = event.target.elements;
        try {
            await app
                .auth()
                .createUserWithEmailAndPassword(email.value, password.value);
            history.push("/home");
        } catch (error) {
            alert(error);
        }
    }, [history]);

    return (
        <div className = "signUpForm">
            <MDBContainer>
                <MDBRow>
                    <MDBCol md="6">
                        <MDBCard>
                            <MDBCardBody>
                                <MDBCardHeader className="form-header deep-blue-gradient rounded">
                                    <h3 className="my-3">
                                        <MDBIcon icon="lock" /> Register new account:
                                    </h3>
                                </MDBCardHeader>
                                <form onSubmit={handleSignUp}>
                                    <div className="grey-text">
                                        <MDBInput label="Type your email" group type="email" name="email" validate error="wrong" success="right" />
                                        <MDBInput label="Type your password" group type="password" name="password" validate />
                                    </div>

                                    <div className="text-center mt-4">
                                        <MDBBtn color="light-blue" className="mb-3" type="submit" >
                                            Register
                                        </MDBBtn>
                                    </div>
                                </form>
                                <MDBModalFooter>
                                    <div className="font-weight-light">
                                        <p> <MDBBtn onClick={() => history.push("/login")}>Log in</MDBBtn></p>
                                    </div>
                                </MDBModalFooter>
                            </MDBCardBody>
                        </MDBCard>
                    </MDBCol>
                </MDBRow>
            </MDBContainer>
        </div>
    );
};

export default withRouter(SignUp);