import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Games } from './components/Games';
import { About } from './components/About';
import { Ranking } from './components/Ranking';
import { AuthProvider } from './Auth';
import Home from './components/Home';
import Login from './components/Login';
import SignUp from './components/SignUp';
import Favorites from './components/Favorites';
import PrivateRoute  from './PrivateRoute';

export default class App extends Component {
    static displayName = App.name;

    render() {
        return (
            <AuthProvider>
                <Layout>
                    <PrivateRoute exact path='/' component={Home} />
                    <PrivateRoute exact path="/home" component={Home} />
                    <Route exact path="/login" component={Login} />
                    <Route exact path="/signup" component={SignUp} />
                    <Route path='/games' component={Games} />
                    <Route path='/about' component={About} />
                    <Route path='/rankings' component={Ranking} />
                    <Route path='/favorites' component={Favorites} />
                </Layout>
            </AuthProvider>
        );
    }
}


