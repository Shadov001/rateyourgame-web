using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FireSharp.Config;
using FireSharp.Interfaces;
using FireSharp.Response;
using Rate_Your_Game.Models;
using Newtonsoft.Json;

namespace Rate_Your_Game.Controllers
{
    [Route("api/[controller]")]
    public class GameController : Controller
    {

        IFirebaseConfig config = new FirebaseConfig
        {
            AuthSecret = APIKeys.AuthSecret,
            BasePath = APIKeys.BasePath
        };

        IFirebaseClient client;

        [HttpPost("[action]")]
        public async void PostGame([FromBody]Game game)
        {
            try
            {
                client = new FireSharp.FirebaseClient(config);
                PushResponse response = client.Push("Games/", game);
                SetResponse setResponse = await client.SetTaskAsync("Games/" + game.Id, game);
                Game result = response.ResultAs<Game>();
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        [HttpGet("[action]")]
        public async Task<Game[]> GetGames()
        {
            try
            {
                client = new FireSharp.FirebaseClient(config);
                FirebaseResponse response = await client.GetTaskAsync("Games/");
                Game[] games = JsonConvert.DeserializeObject<Game[]>(response.Body);  //serialize json to list of Game objects
                return games;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

    }
}
