using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FireSharp.Config;
using FireSharp.Interfaces;
using FireSharp.Response;
using Rate_Your_Game.Models;

namespace Rate_Your_Game.Controllers
{
    [Route("api/[controller]")]
    public class DeliveryAddressController : Controller
    {

        IFirebaseConfig config = new FirebaseConfig
        {
            AuthSecret = APIKeys.AuthSecret,
            BasePath= APIKeys.BasePath
        };

        IFirebaseClient client;

        [HttpPost("[action]")]
        public async void PostDeliveryAddress([FromBody]DeliveryAddress address)
        {
            try
            {
                client = new FireSharp.FirebaseClient(config);
                PushResponse response = client.Push("DeliveryAddresses/", address);
                SetResponse setResponse = await client.SetTaskAsync("DeliveryAddresses/" + address.Id, address);
                DeliveryAddress result = response.ResultAs<DeliveryAddress>();
            }
            catch (Exception e)
            {
                throw e;
            }

        }

    }
}
