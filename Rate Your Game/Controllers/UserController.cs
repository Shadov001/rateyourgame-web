using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FireSharp.Config;
using FireSharp.Interfaces;
using FireSharp.Response;
using Rate_Your_Game.Models;

namespace Rate_Your_Game.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {

        IFirebaseConfig config = new FirebaseConfig
        {
            AuthSecret = APIKeys.AuthSecret,
            BasePath = APIKeys.BasePath
        };

        IFirebaseClient client;

        [HttpPost("[action]")]
        public async void PostUser([FromBody]User user)
        {
            try
            {
                client = new FireSharp.FirebaseClient(config);
                PushResponse response = client.Push("Users/", user);
                SetResponse setResponse = await client.SetTaskAsync("Users/" + user.Id, user);
                User result = response.ResultAs<User>();
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        //TODO: GET(user_id) => retrieve all data about current user

    }
}
