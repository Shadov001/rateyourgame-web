﻿using FireSharp.Config;
using FireSharp.Interfaces;
using FireSharp.Response;
using Microsoft.AspNetCore.Mvc;
using Rate_Your_Game.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Rate_Your_Game.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ForecastController : ControllerBase
    {
        // GET: api/Forecast
        IFirebaseConfig config = new FirebaseConfig
        {
            AuthSecret = APIKeys.AuthSecret,
            BasePath = APIKeys.BasePath
        };

        IFirebaseClient client;

        private static string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        [HttpGet("[action]")]
        public IEnumerable<WeatherForecast> WeatherForecasts()
        {
            try
            {
                client = new FireSharp.FirebaseClient(config);
            }
            catch (Exception e)
            {
                throw e;
            }

            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                DateFormatted = DateTime.Now.AddDays(index).ToString("d"),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            });
        }

        [HttpPost("[action]")]
        public async void PostForecast([FromBody]WeatherForecast forecast)
        {
            try
            {
                client = new FireSharp.FirebaseClient(config);
                PushResponse response = client.Push("Forecast/", forecast);
                SetResponse setResponse = await client.SetTaskAsync("Forecast/" + forecast.DateFormatted, forecast);
                WeatherForecast result = response.ResultAs<WeatherForecast>();
            }
            catch (Exception e)
            {
                throw e;
            }

        }
    }
}
