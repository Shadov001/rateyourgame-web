using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FireSharp.Config;
using FireSharp.Interfaces;
using FireSharp.Response;
using Rate_Your_Game.Models;

namespace Rate_Your_Game.Controllers
{
    [Route("api/[controller]")]
    public class LanguageController : Controller
    {

        IFirebaseConfig config = new FirebaseConfig
        {
            AuthSecret = APIKeys.AuthSecret,
            BasePath = APIKeys.BasePath
        };

        IFirebaseClient client;

        [HttpPost("[action]")]
        public async void PostLanguage([FromBody]Language language)
        {
            try
            {
                client = new FireSharp.FirebaseClient(config);
                PushResponse response = client.Push("Languages/", language);
                SetResponse setResponse = await client.SetTaskAsync("Languages/" + language.Id, language);
                Language result = response.ResultAs<Language>();
            }
            catch (Exception e)
            {
                throw e;
            }

        }

    }
}
