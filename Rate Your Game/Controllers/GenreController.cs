using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FireSharp.Config;
using FireSharp.Interfaces;
using FireSharp.Response;
using Rate_Your_Game.Models;

namespace Rate_Your_Game.Controllers
{
    [Route("api/[controller]")]
    public class GenreController : Controller
    {

        IFirebaseConfig config = new FirebaseConfig
        {
            AuthSecret = APIKeys.AuthSecret,
            BasePath = APIKeys.BasePath
        };

        IFirebaseClient client;


        [HttpPost("[action]")]
        public async void PostGenre([FromBody]Genre genre)
        {
            try
            {
                client = new FireSharp.FirebaseClient(config);
                PushResponse response = client.Push("Genres/", genre);
                SetResponse setResponse = await client.SetTaskAsync("Genres/" + genre.Id, genre);
                Genre result = response.ResultAs<Genre>();
            }
            catch (Exception e)
            {
                throw e;
            }

        }
    }
}
