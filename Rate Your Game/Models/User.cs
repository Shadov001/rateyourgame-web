﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rate_Your_Game.Models
{
    public class User
    {
        public int Id { get; set; }
        public bool IsModerator { get; set; }
        public bool IsAdmin { get; set; }
        public string Nickname { get; set; }
        public string Password { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string FullName
        {
            get
            {
                return Firstname + Lastname;
            }
        }
        public DeliveryAddress Address { get; set; }
        public DateTime RegistrationDate { get; set; }
        public double TrustFactor { get; set; }   //property that stores factor of trust of certain user 

    }
}
