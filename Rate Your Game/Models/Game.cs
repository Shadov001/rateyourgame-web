﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rate_Your_Game.Models
{
    public class Game
    {
        public Game() {}

        public int Id { get; set; }
        public string Title { get; set; }           //game title
        public string Description { get; set; }     //short description
        public DateTime ReleaseDate { get; set; }   //date of release
    /*    public Genre[] Genres { get; set; }      */   //genres of game (fps,action,simulator)  
        public string Genre { get; set; }
        public double Rating { get; set; }          //overall rating of game
        public int Views { get; set; }              //views of game
        public string StudioName { get; set; }      //developer
        public Language[] Languages { get; set; }   // language of game
        
    }
}
