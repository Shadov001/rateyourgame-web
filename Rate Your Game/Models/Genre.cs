﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rate_Your_Game.Models
{
    public class Genre
    {
        public int Id { get; set; }
        public string Value { get; set; }

    }
}
